.. Django PFX documentation master file, created by
   sphinx-quickstart on Fri Dec 10 14:58:11 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Django PFX's documentation!
======================================

.. include:: ../README.md
   :parser: myst_parser.sphinx_

Topics
======

.. toctree::
   :maxdepth: 2

   source/getting_started
   source/decorator
   source/pfx_views
   source/model
   source/internationalisation
   source/authentication
   source/settings
   source/testing
   source/profiling
   source/generate_openapi
   source/api.views

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
