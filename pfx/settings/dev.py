SECRET_KEY = 'fake-key'
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.postgres',
    'pfx.pfxcore',
]
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
