"""
Custom development settings example.

To use custom settting please copy this file:
`cp dev_example.py dev_custom.py to create it.`
"""

from .dev_default import *  # noqa

# SECRET_KEY = 'fake-key'
# INSTALLED_APPS = [
#     'django.contrib.contenttypes',
#     'tests',
# ]
#
# DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
#
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'test-pfx',
#         'USER': 'django',
#         'PASSWORD': 'django',
#         'HOST': 'localhost',
#         'PORT': '5432',
#     }
# }
# ROOT_URLCONF = 'tests.urls'
